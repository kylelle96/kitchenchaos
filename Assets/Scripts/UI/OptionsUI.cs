using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class OptionsUI : MonoBehaviour
{
    public static OptionsUI Instance { get; private set; }

    [SerializeField] Button soundEffectsButton;
    [SerializeField] Button musicButton;
    [SerializeField] Button closeButton;
    [SerializeField] Button moveUpButton;
    [SerializeField] Button moveDownButton;
    [SerializeField] Button moveLeftButton;
    [SerializeField] Button moveRightButton;
    [SerializeField] Button interactButton;
    [SerializeField] Button interactAltButton;
    [SerializeField] Button pauseButton;
    [SerializeField] TextMeshProUGUI soundEffectsText;
    [SerializeField] TextMeshProUGUI musicText;
    [SerializeField] TextMeshProUGUI moveUpBindText;
    [SerializeField] TextMeshProUGUI moveDownBindText;
    [SerializeField] TextMeshProUGUI moveLeftBindText;
    [SerializeField] TextMeshProUGUI moveRightBindText;
    [SerializeField] TextMeshProUGUI interactBindText;
    [SerializeField] TextMeshProUGUI interactAltBindText;
    [SerializeField] TextMeshProUGUI pauseBindText;
    [SerializeField] Transform pressToRebindKeyTransform;


    private void Awake() {
        
        Instance = this;

        soundEffectsButton.onClick.AddListener(() => {
            SoundManager.Instance.ChangeVolume();
            UpdateVisual();

        });
        musicButton.onClick.AddListener(() => {  
            MusicManager.Instance.ChangeVolume();
            UpdateVisual();
        });

        closeButton.onClick.AddListener(() => {
            Hide();
        });

        moveUpButton.onClick.AddListener(() => { RebindBinding(GameInput.Binding.Move_Up); });
        moveDownButton.onClick.AddListener(() => { RebindBinding(GameInput.Binding.Move_Down); });
        moveLeftButton.onClick.AddListener(() => { RebindBinding(GameInput.Binding.Move_Left); ; });
        moveRightButton.onClick.AddListener(() => { RebindBinding(GameInput.Binding.Move_Right); });
        interactButton.onClick.AddListener(() => { RebindBinding(GameInput.Binding.Interact); });
        interactAltButton.onClick.AddListener(() => { RebindBinding(GameInput.Binding.InteractAlternate); });
        pauseButton.onClick.AddListener(() => { RebindBinding(GameInput.Binding.Pause); });
    }

    private void Start() {

        KitchenGameManager.Instance.OnUnpausedGame += KitchenGameManager_OnUnpausedGame;


        UpdateVisual();

        HideRebindKeyMessage();
        Hide();
    }

    private void KitchenGameManager_OnUnpausedGame(object sender, System.EventArgs e) {
        Hide();
    }

    private void UpdateVisual() {
        soundEffectsText.text = "Sound Effects: " + Mathf.Round(SoundManager.Instance.GetVolume() * 10f);
        musicText.text = "Music: " + Mathf.Round(MusicManager.Instance.GetVolume() * 10f);
        moveUpBindText.text = GameInput.Instance.GetBindingText(GameInput.Binding.Move_Up);
        moveDownBindText.text = GameInput.Instance.GetBindingText(GameInput.Binding.Move_Down);
        moveLeftBindText.text = GameInput.Instance.GetBindingText(GameInput.Binding.Move_Left);
        moveRightBindText.text = GameInput.Instance.GetBindingText(GameInput.Binding.Move_Right);
        interactBindText.text = GameInput.Instance.GetBindingText(GameInput.Binding.Interact);
        interactAltBindText.text = GameInput.Instance.GetBindingText(GameInput.Binding.InteractAlternate);
        pauseBindText.text = GameInput.Instance.GetBindingText(GameInput.Binding.Pause);
    }

    public void Show() {
        gameObject.SetActive(true);
    }

    public void Hide() {
        gameObject.SetActive(false);
    }

    public void ShowRebindKeyMessage() {
        pressToRebindKeyTransform.gameObject.SetActive(true);
    }

    public void HideRebindKeyMessage() {
        pressToRebindKeyTransform.gameObject.SetActive(false);
    }

    public void RebindBinding(GameInput.Binding binding) {
        ShowRebindKeyMessage();
        GameInput.Instance.RebindBinding(binding, () => {
            HideRebindKeyMessage();
            UpdateVisual();
            });
    }
}
