using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicManager : MonoBehaviour
{
    public const string PLAYER_PREF_MUSIC_VOLUME = "MusicVolume";
    public static MusicManager Instance { get; private set; }

    private AudioSource audioSource;
    private float volume = 0.5f;

    private void Awake() {
        Instance = this; 

        audioSource = GetComponent<AudioSource>();

        volume = PlayerPrefs.GetFloat(PLAYER_PREF_MUSIC_VOLUME, 1f);
        audioSource.volume = volume;
    }

    public void ChangeVolume() {
        volume += .1f;
        if(volume > 1.01f) {
            volume = 0f;
        }
        PlayerPrefs.SetFloat(PLAYER_PREF_MUSIC_VOLUME, volume);
        PlayerPrefs.Save();
        audioSource.volume = volume;
    }

    public float GetVolume() {
        return volume;
    }


}
