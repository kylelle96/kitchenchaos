using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GamePlayingClockTimerUI : MonoBehaviour
{
    [SerializeField] Image clockImage;

    private void Start() {
        KitchenGameManager.Instance.OnStateChanged += KitchenGameManager_OnStateChanged;

        Hide();
    }

    private void KitchenGameManager_OnStateChanged(object sender, System.EventArgs e) {
        if (KitchenGameManager.Instance.IsGamePlaying()) {
            Show();
        }
        else {
            Hide();
        }
    }

    // Update is called once per frame
    void Update()
    {
        clockImage.fillAmount = KitchenGameManager.Instance.GetGamePlayingTimerNormalized();
    }

    private void Show() {
        gameObject.SetActive(true);
    }

    private void Hide() {
        gameObject.SetActive(false);
    }
}
