using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GamePauseUI : MonoBehaviour
{
    [SerializeField] Button resumeButton;
    [SerializeField] Button optionsButton;
    [SerializeField] Button mainMenuButton;

    private void Start() {
        KitchenGameManager.Instance.OnPausedGame += KitchenGameManager_OnPausedGame;
        KitchenGameManager.Instance.OnUnpausedGame += KitchenGameManager_OnUnpausedGame;

        resumeButton.onClick.AddListener(() => {
            KitchenGameManager.Instance.TogglePauseGame();
        });

        optionsButton.onClick.AddListener(() => {
            OptionsUI.Instance.Show();
        });

        mainMenuButton.onClick.AddListener(() => {
            Loader.Load(Loader.Scene.MainMenuScene);
        });

        Hide();
    }

    //private void OnDestroy() {
    //    KitchenGameManager.Instance.OnPausedGame -= KitchenGameManager_OnPausedGame;
    //    KitchenGameManager.Instance.OnUnpausedGame -= KitchenGameManager_OnUnpausedGame;
    //}

    private void KitchenGameManager_OnUnpausedGame(object sender, System.EventArgs e) {
        Hide();
    }

    private void KitchenGameManager_OnPausedGame(object sender, System.EventArgs e) {
        Show();
    }

    private void Show() {
        gameObject.SetActive(true);
    }

    private void Hide() {
        gameObject.SetActive(false);
    }


}
