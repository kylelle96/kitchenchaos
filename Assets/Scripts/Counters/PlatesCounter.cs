using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatesCounter : BaseCounter
{
    public event EventHandler OnPlateSpawned;
    public event EventHandler OnPlateRemoved;

    [SerializeField] KitchenObjectSO kitchenObjectSO;
    private float spawnPlateTimer;
    private float spawnPlateTimerMax = 4f;

    private int plateSpawnedAmount = 0;
    private int plateSpawnedAmountMax = 4;

    private void Update() {
        spawnPlateTimer += Time.deltaTime;

        if(spawnPlateTimer > spawnPlateTimerMax) {

            spawnPlateTimer = 0;

            if(plateSpawnedAmount < plateSpawnedAmountMax) {
                plateSpawnedAmount++;
                OnPlateSpawned?.Invoke(this,EventArgs.Empty);
            }
        }
    }

    public override void Interact(Player player) {
        if (!player.HasKitchenObject()) {
            //Player is empty handed
            if(plateSpawnedAmount > 0) {
                plateSpawnedAmount--;

                KitchenObject.SpawnKitchenObject(kitchenObjectSO, player);

                OnPlateRemoved?.Invoke(this,EventArgs.Empty);
            }
        }
    }
}
