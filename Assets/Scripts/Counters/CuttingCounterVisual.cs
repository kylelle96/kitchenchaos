using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CuttingCounterVisual : MonoBehaviour
{
    public const string CUT = "Cut";

    [SerializeField] private Animator animator;
    [SerializeField] CuttingCounter cuttingCounter;


    private void Start() {
        cuttingCounter.OnPlayerCutting += CuttingCounter_OnPlayerCutting;
    }

    private void CuttingCounter_OnPlayerCutting(object sender, System.EventArgs e) {
        animator.SetTrigger(CUT);
    }
}
