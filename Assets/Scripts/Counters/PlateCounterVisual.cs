using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlateCounterVisual : MonoBehaviour
{
    [SerializeField] PlatesCounter plateCounter;
    [SerializeField] Transform counterTopPoint;
    [SerializeField] Transform plateVisualPrefab;

    private List<GameObject> spawnedPlateGameObjectList;

    private void Awake() {
        spawnedPlateGameObjectList = new List<GameObject>();
    }

    private void Start() {
        plateCounter.OnPlateSpawned += PlateCounter_OnPlateSpawned;
        plateCounter.OnPlateRemoved += PlateCounter_OnPlateRemoved;
    }

    private void PlateCounter_OnPlateRemoved(object sender, System.EventArgs e) {
        GameObject plateGameObject = spawnedPlateGameObjectList[spawnedPlateGameObjectList.Count - 1];
        spawnedPlateGameObjectList.Remove(plateGameObject);
        Destroy(plateGameObject);
    }

    private void PlateCounter_OnPlateSpawned(object sender, System.EventArgs e) {
        Transform plateVisualTransform = Instantiate(plateVisualPrefab, counterTopPoint);

        float plateYOffset = 0.1f;
        plateVisualTransform.localPosition = new Vector3(0, plateYOffset * spawnedPlateGameObjectList.Count, 0);

        spawnedPlateGameObjectList.Add(plateVisualTransform.gameObject);

    }
}
