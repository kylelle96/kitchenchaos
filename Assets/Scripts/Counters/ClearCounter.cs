using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClearCounter : BaseCounter
{
    [SerializeField] private KitchenObjectSO kitchenObjectSO;


    public override void Interact(Player player) {
        if (!HasKitchenObject()) {
            //Counter has no kitchen Object on them
            if (player.HasKitchenObject()) {
                //player has a kitchen Object
                player.GetKitchenObject().SetKitchenObjectParent(this);
            }
            else {
                //player has no kitchen Object
            }
        }
        else {
            //Counter has kitchen object on them
            if (player.HasKitchenObject()) {
                //player has kitchen Object
                if(player.GetKitchenObject().TryGetPlate(out PlateKitchenObject plateKitchenObject)) {
                    //player is holding a Plate
                    if (plateKitchenObject.TryAddIngredient(GetKitchenObject().GetKitchenObjectSO())){
                        GetKitchenObject().DestroySelf();
                    }
                }
                else {
                    //player is not carrying Plate but something else
                    if(GetKitchenObject().TryGetPlate(out plateKitchenObject)) {
                        //Counter is holding a Plate
                        if (plateKitchenObject.TryAddIngredient(player.GetKitchenObject().GetKitchenObjectSO())) {
                            player.GetKitchenObject().DestroySelf();
                        }
                    }
                }
            }
            else {
                //player has no kitchen Object
                GetKitchenObject().SetKitchenObjectParent(player);
            }
            
        }
    }
}
