using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static CuttingCounter;

public class StoveCounter : BaseCounter, IHasProgress {

    public event EventHandler<OnStateChangedEventArgs> OnStateChanged;
    public event EventHandler<IHasProgress.OnProgressChangedEventArgs> OnProgressChanged;

    public class OnStateChangedEventArgs : EventArgs {
        public State state;
    }

    public enum State {
        Idle,
        Frying,
        Fried,
        Burned
    }
    
    
    [SerializeField] FryingRecipeSO[] fryingRecipeSOArray;
    [SerializeField] BurnedRecipeSO[] burnedRecipeSOArray;

    private float fryingTimer;
    private FryingRecipeSO fryingRecipeSO;
    private float burnedTimer;
    private BurnedRecipeSO burnedRecipeSO;
    private State state;

    private void Start() {
        state = State.Idle;
    }

    private void Update() {

        if (HasKitchenObject()) {
            switch (state) {
                case State.Idle:
                    break;
                case State.Frying:
                    fryingTimer += Time.deltaTime;
                    OnProgressChanged?.Invoke(this, new IHasProgress.OnProgressChangedEventArgs {
                        progressNormalized = fryingTimer / fryingRecipeSO.fryingTimerMax
                    });

                    if (fryingTimer > fryingRecipeSO.fryingTimerMax) {
                        //Fried
                        fryingTimer = 0;
                        GetKitchenObject().DestroySelf();

                        KitchenObject.SpawnKitchenObject(fryingRecipeSO.output, this);

                        burnedRecipeSO = GetBurnedRecipeSOWithInput(GetKitchenObject().GetKitchenObjectSO());
                        state = State.Fried;
                        OnStateChanged?.Invoke(this, new OnStateChangedEventArgs {
                            state = GetCurrentState()
                        });

                    }
                    break;
                case State.Fried:
                    burnedTimer += Time.deltaTime;
                    OnProgressChanged?.Invoke(this, new IHasProgress.OnProgressChangedEventArgs {
                        progressNormalized = burnedTimer / burnedRecipeSO.burningTimerMax
                    });

                    if (burnedTimer > burnedRecipeSO.burningTimerMax) {
                        //Fried
                        burnedTimer = 0;
                        GetKitchenObject().DestroySelf();

                        KitchenObject.SpawnKitchenObject(burnedRecipeSO.output, this);

                        state = State.Burned;
                        OnStateChanged?.Invoke(this, new OnStateChangedEventArgs {
                            state = GetCurrentState()
                        });

                        OnProgressChanged?.Invoke(this, new IHasProgress.OnProgressChangedEventArgs {
                            progressNormalized = 0f
                        });
                    }
                    break;
                case State.Burned:
                    break;
            }
            //Debug.Log(state);
        }
    }



    public override void Interact(Player player) {
        if (!HasKitchenObject()) {
            //Counter has no kitchen Object on them
            if (player.HasKitchenObject()) {
                //player has a kitchen Object
                if (HasRecipeWithInput(player.GetKitchenObject().GetKitchenObjectSO())) {
                    //Player carry something that can be Fried
                    player.GetKitchenObject().SetKitchenObjectParent(this);

                    fryingRecipeSO = GetFryingRecipeSOWithInput(GetKitchenObject().GetKitchenObjectSO());
                    state = State.Frying;
                    OnStateChanged?.Invoke(this, new OnStateChangedEventArgs {
                        state = GetCurrentState()
                    });
                    OnProgressChanged?.Invoke(this, new IHasProgress.OnProgressChangedEventArgs {
                        progressNormalized = fryingTimer / fryingRecipeSO.fryingTimerMax
                    });
                }
            }
            else {
                //player has no kitchen Object
            }
        }
        else {
            //Counter has kitchen object on them
            if (player.HasKitchenObject()) {
                //player has kitchen Object
                if (player.GetKitchenObject().TryGetPlate(out PlateKitchenObject plateKitchenObject)) {
                    //player is holding a Plate
                    if (plateKitchenObject.TryAddIngredient(GetKitchenObject().GetKitchenObjectSO())) {
                        GetKitchenObject().DestroySelf();

                        state = State.Idle;
                        fryingTimer = 0;
                        burnedTimer = 0;
                        OnStateChanged?.Invoke(this, new OnStateChangedEventArgs {
                            state = GetCurrentState()
                        });

                        OnProgressChanged?.Invoke(this, new IHasProgress.OnProgressChangedEventArgs {
                            progressNormalized = 0f
                        });
                    }
                }
            }
            else {
                //player has no kitchen Object
                GetKitchenObject().SetKitchenObjectParent(player);
                state = State.Idle;
                fryingTimer = 0;
                burnedTimer = 0;
                OnStateChanged?.Invoke(this, new OnStateChangedEventArgs {
                    state = GetCurrentState()
                });

                OnProgressChanged?.Invoke(this, new IHasProgress.OnProgressChangedEventArgs {
                    progressNormalized = 0f
                });
            }
        }


    }

    private KitchenObjectSO GetOutputForInput(KitchenObjectSO inputKitchenObject) {

        FryingRecipeSO fryingRecipeSO = GetFryingRecipeSOWithInput(inputKitchenObject);

        if (fryingRecipeSO != null) {
            return fryingRecipeSO.output;
        }
        else {
            return null;
        }
    }

    private bool HasRecipeWithInput(KitchenObjectSO inputKitchenObject) {

        FryingRecipeSO fryingRecipeSO = GetFryingRecipeSOWithInput(inputKitchenObject);

        return fryingRecipeSO != null;
    }

    private FryingRecipeSO GetFryingRecipeSOWithInput(KitchenObjectSO inputKitchenObject) {
        foreach (FryingRecipeSO fryingRecipeSO in fryingRecipeSOArray) {
            if (fryingRecipeSO.input == inputKitchenObject) {
                return fryingRecipeSO;
            }
        }

        return null;
    }


    private BurnedRecipeSO GetBurnedRecipeSOWithInput(KitchenObjectSO inputKitchenObject) {
        foreach (BurnedRecipeSO burnedRecipeSO in burnedRecipeSOArray) {
            if (burnedRecipeSO.input == inputKitchenObject) {
                return burnedRecipeSO;
            }
        }

        return null;
    }

    private State GetCurrentState() {
        return state;
    }
}
