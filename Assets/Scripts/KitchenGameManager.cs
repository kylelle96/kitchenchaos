using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KitchenGameManager : MonoBehaviour
{
    public event EventHandler OnStateChanged;
    public event EventHandler OnPausedGame;
    public event EventHandler OnUnpausedGame;

    public static KitchenGameManager Instance { get; private set; }

    public enum GameState {
        WaitingToStart,
        CountdownToStart,
        GamePlaying,
        GameOver,
    }

    private GameState gameState;
    private float waitingToStartTimer = 3f;
    private float gameCountdownTimer = 3f;
    private float gamePlayingTimer;
    private float gamePlayingTimerMax = 120f;
    private bool isGamePaused = false;

    private void Awake() {
        Instance = this;
        gameState = GameState.WaitingToStart;
    }

    private void Start() {
        GameInput.Instance.OnPauseAction += GameInput_OnPauseAction;
    }

    private void GameInput_OnPauseAction(object sender, EventArgs e) {
        TogglePauseGame();
    }

    private void Update() {
        switch (gameState) {
            case GameState.WaitingToStart:
                waitingToStartTimer -= Time.deltaTime;
                if(waitingToStartTimer < 0) {
                    gameState = GameState.CountdownToStart;
                    OnStateChanged?.Invoke(this, EventArgs.Empty);
                }
                break;
            case GameState.CountdownToStart:
                gameCountdownTimer -= Time.deltaTime;
                if(gameCountdownTimer < 0) {
                    gameState = GameState.GamePlaying;
                    gamePlayingTimer = gamePlayingTimerMax;
                    OnStateChanged?.Invoke(this, EventArgs.Empty);
                }
                break;
            case GameState.GamePlaying:
                gamePlayingTimer -= Time.deltaTime;
                if(gamePlayingTimer < 0) {
                    gameState  = GameState.GameOver;
                    OnStateChanged?.Invoke(this, EventArgs.Empty);
                }

                break;
            case GameState.GameOver:
                OnStateChanged?.Invoke(this, EventArgs.Empty);
                break;
        }
    }

    public bool IsGamePlaying() {
        return gameState == GameState.GamePlaying;
    }
    
    public bool IsCountdownToStartActive() {
        return gameState == GameState.CountdownToStart;
    }

    public bool IsGameOver() {
        return gameState == GameState.GameOver;
    }
    
    public float GetCountdownTimer() {
        return gameCountdownTimer;
    }

    public float GetGamePlayingTimerNormalized() {
        return 1f - (gamePlayingTimer / gamePlayingTimerMax);
    }

    public void TogglePauseGame() {
        isGamePaused = !isGamePaused;
        if (isGamePaused) {
            Time.timeScale = 0f;
            OnPausedGame?.Invoke(this, EventArgs.Empty);
        }
        else {
            Time.timeScale = 1f;
            OnUnpausedGame?.Invoke(this, EventArgs.Empty);
        }
    }
}
